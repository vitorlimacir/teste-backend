const Movie = require('../models/movie')

const find = async (req, res) => {
  try {
    const movie = await Movie.findOne({ _id: req.params.id }).populate('votes.userId')
    return res.status(200).send(movie)
  } catch (e) {
    console.log(e)
    return res.status(500).send({ error: true, message: "Não foi possível encontrar o filme" })
  }
}

const findAll = async (req, res) => {
  try {
    const movie = await Movie.find().populate('votes.userId')
    return res.status(200).send(movie)
  } catch (e) {
    console.log(e)
    return res.status(500).send({ error: true, message: "Não foi possível encontrar os filmes" })
  }
}


const create = async (req, res) => {
  try {
    const movie = new Movie(req.body)
    const saved = await movie.save()
    return res.status(200).send(saved)
  } catch (e) {
    console.log(e)
    return res.status(500).send({ error: true, message: "Não foi possível atualizar o filme" })
  }
}

const vote = async (req, res) => {
  try {
    return Movie.findOne({ _id: req.params.id }, async (err, movie) => {
      let user = movie.votes.find(e => e.userId == req.userId)
      if(user) await user.remove()
      if(!req.body.vote || (req.body.vote && req.body.vote > 5)) return res.status(403).send({ message: 'Voto inválido' })
      movie.votes.push({userId: req.userId, vote: req.body.vote})
      const updated = await movie.save()
      return res.status(200).send(updated)
    })
  } catch (err) {
    console.log(err)
    return res.status(500).send({ error: true, message: "Não foi possível votar no filme" })
  }
}

module.exports = {
  find,
  findAll,
  create,
  vote
}