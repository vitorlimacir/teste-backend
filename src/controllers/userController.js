const User = require('../models/user')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')


const generateToken = (params = {}) => {
  return jwt.sign(params, process.env.SECRET_KEY, { expiresIn: 86400 })
}

const register = async (req, res, next) => {
  const { email } = req.body
  if (await User.findOne({ email }))
    return res.status(400).send({ error: 'Usuário já existe' })

  const user = await User.create(req.body)
  user.password = undefined

  return res.send({ user, token: generateToken({ id: user._id, role: user.role }) })
}

const authenticate = async (req, res, next) => {
  const { email, password } = req.body
  const user = await User.findOne({ email })
  if (!user) return res.status(400).send({ error: 'Usuário não encontrado' })

  if (!await bcrypt.compare(password, user.password))
    return res.status(400).send({ error: 'Senha incorreta' })

  user.password = undefined
  res.send({ user, token: generateToken({ id: user._id, role: user.role }) })
}


const find = async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.params.id })
    return res.status(200).send(user)
  } catch (e) {
    console.log(e)
    return res.status(500).send({ error: true, message: "Não foi possível encontrar o usuário" })
  }
}

const findAll = async (req, res) => {
  try {
    const user = await User.find()
    return res.status(200).send(user)
  } catch (e) {
    console.log(e)
    return res.status(500).send({ error: true, message: "Não foi possível encontrar os usuários" })
  }
}


const update = async (req, res) => {
  try {
    const updated = await User.findByIdAndUpdate(req.params.id, req.body, { new: true })
    return res.status(200).send(updated)
  } catch (e) {
    console.log(e)
    return res.status(500).send({ error: true, message: "Não foi possível atualizar o usuário" })
  }
}

const remove = async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.params.id })
    user.active = false
    await user.save()
    return res.status(200).send({ message: 'Usuário Removido' })
  } catch (e) {
    console.log(e)
    return res.status(500).send({ error: true, message: "Não foi possível remover o usuário" })
  }
}

module.exports = {
  find,
  findAll,
  register,
  authenticate,
  update,
  remove
}