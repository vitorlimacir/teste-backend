const jwt = require('jsonwebtoken')

module.exports = (roles = []) => {
  if (typeof roles === 'string') {
    roles = [roles]
  }
  return (req, res, next) => {
    const authHeader = req.headers.authorization
    if (!authHeader) return res.status(401).send({ message: 'Não Autorizado' })
  
    const [scheme, token] = authHeader.split(' ')
    
    jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
      if(err) return res.status(401).send({ message: 'Token inválido' })
  
      if (roles.length && !roles.includes(decoded.role)) {
        return res.status(401).send({ message: 'Não Autorizado' })
      }
  
      req.userId = decoded.id
      next()
    })
  }
}