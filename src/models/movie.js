const mongoose = require('mongoose')

const votesScema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  vote: {
    type: Number,
    required: true
  }
})

const MovieSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  year: {
    type: Number,
    required: true
  },
  author: {
    type: String,
    required: true
  },
  votes: [votesScema]
}, {
  timestamps: true
})

MovieSchema.virtual('votesAverage').get(function() {
  return this.votes.reduce((acc, cur) => acc += cur.vote, 0)/this.votes.length
})

MovieSchema.set('toJSON', {
  virtuals: true
})

module.exports = mongoose.model('Movie', MovieSchema)