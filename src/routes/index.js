const {Router} = require('express')
const routes = Router()

const auth = require('../middlewares/auth')

const userController = require('../controllers/userController')
const movieController = require('../controllers/movieController')

// Admin
routes.get('/users', auth('admin'), userController.findAll)
routes.get('/users/:id', auth('admin'), userController.find)
routes.post('/movies', auth('admin'), movieController.create)

// Users
routes.post('/register', userController.register)
routes.post('/login', userController.authenticate)
routes.put('/users/:id', auth(), userController.update)
routes.delete('/users/:id',auth(), userController.remove)

//Movies
routes.get('/movies', auth(), movieController.findAll)
routes.get('/movies/:id', auth(), movieController.find)
routes.put('/movies/:id/vote',auth(), movieController.vote)

module.exports = routes 