# :movie_camera: IMDB API Clone

***
### Instale as dependências

#### `npm install`



### Edite o aquivo .env.exemple:

Você precisa especificar a url de conexão com o banco MongoDB, local ou cloud.

Renomeie o aquivo de ".env.exemple" para ".env"

### API está pronta! você pode roda-lá:

#### `npm run dev`

***
## Rotas Públicas

Rotas                      |     Métodos HTTP   |      Descrição              |      Links                 
-------------------------  | ----------------- | ---------------------         | ---------------------------------------- 
/register                  |       POST        | Registra um usuário       | POST:    http://localhost:3000/register      
/login                     |       POST        | Autentica um usuário       | POST:   http://localhost:3000/login

Para registrar um usuário ADMIN, basta enviar role: "admin" junto com o objeto usuário. 
## Rotas  Usuário

Rotas                      |     Métodos HTTP   |      Descrição              |      Links                 
-------------------------  | ----------------- | ---------------------         | ---------------------------------------- 
/users/:id                 |       PUT         | Edita um usuário            | PUT:    http://localhost:3000/users/:id 
/users/:id                 |       DELETE      | Desabilita um usuário    | DELETE:    http://localhost:3000/users/:id 
/movies                    |       GET         | Lista todos filmes cadastrados    | GET:    http://localhost:3000/movies
/movies/:id                |       GET         | Lista um filme cadastrado    | GET:    http://localhost:3000/movies/:id
/movies/:id/vote           |       PUT         | Vota em um filme cadastrado     | PUT:    http://localhost:3000/movies/:id/vote  

## Rotas Admin

Rotas                      |     Métodos HTTP  |      Descrição              |      Links                 
-------------------------  | ----------------- | ---------------------         | ---------------------------------------- 
/users                     |       GET         | Lista todos usuários cadastrados       | GET:    http://localhost:3000      
/users/:id                 |       GET         | Lista o usuário cadastrado        | GET:   http://localhost:3000/users/:id
/movies                    |       POST        | Cadastro de filme    | POST:    http://localhost:3000/movies